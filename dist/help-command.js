"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("./helpers");
class HelpCommand {
    constructor(helpString, robot) {
        this.robot = robot;
        this.robotName = robot.alias || robot.name;
        this.command = helpers_1.Helpers.parseCommand(helpString, this.robotName);
        this.description = helpers_1.Helpers.parseDescription(helpString, this.robotName);
        this.keywords = helpers_1.Helpers.parseKeywords(helpString);
        this.parameters = helpers_1.Helpers.parseParameters(helpString);
        this.aliases = helpers_1.Helpers.parseAliases(helpString, this.robotName);
    }
    matches(key) {
        const keys = key.replace(/\s+/g, ' ').split(' ');
        const searches = [
            this.command, this.description,
            ...this.keywords, ...this.aliases
        ];
        return helpers_1.Helpers.findInArray(keys, searches);
    }
    isQuery(key) {
        const command = helpers_1.Helpers.prepareToCompare(this.command);
        const query = helpers_1.Helpers.prepareToCompare(key);
        return command === query;
    }
    display() {
        const format = process.env.HELP_COMMAND_FORMAT || '_${command}_ - ${description}';
        return helpers_1.Helpers.format(format, { command: this.command, description: this.description });
    }
    displayDetailed() {
        const infos = [this.display(), this.prepareParametersInfo(), this.prepareAliasesInfo()].filter((el) => {
            return el != '';
        });
        return infos.join("\n");
    }
    prepareParametersInfo() {
        const format = process.env.HELP_PARAMETER_FORMAT || '_${parameter}_ - ${description}';
        let parameters = [];
        for (const parameter of Object.keys(this.parameters)) {
            const param = helpers_1.Helpers.format(format, { parameter, description: this.parameters[parameter] });
            parameters.push(param);
        }
        const header = process.env.HELP_PARAMETERS_HEADER || 'Parameters list:';
        return parameters.length > 0 ? `${header}\n${parameters.join("\n")}` : '';
    }
    prepareAliasesInfo() {
        const header = process.env.HELP_ALIASES_HEADER || 'You can also use this command with:';
        return this.aliases.length > 0 ? `${header}\n${this.aliases.join("\n")}` : '';
    }
}
exports.HelpCommand = HelpCommand;
