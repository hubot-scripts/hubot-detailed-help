"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const help_command_1 = require("./help-command");
const help_tag_1 = require("./help-tag");
class HelpLoader {
    constructor(robot) {
        this.commands = [];
        this.tags = [];
        this.robot = null;
        this.robot = robot;
        const helpCommands = robot.helpCommands();
        for (const helpCommand of helpCommands) {
            if (helpCommand.match(/^TAG\s+\[.*]/)) {
                this.tags.push(new help_tag_1.HelpTag(helpCommand));
            }
            else {
                this.commands.push(new help_command_1.HelpCommand(helpCommand, robot));
            }
        }
        this.mapCommandsToTags();
        robot.logger.debug(`Loaded ${this.commands.length} help commands and ${this.tags.length} help tags.`);
    }
    searchForCommand(key) {
        let result = [];
        for (const command of this.commands) {
            if (command.isQuery(key) && !this.findTagByName(key)) {
                return [command];
            }
            if (command.matches(key)) {
                result.push(command);
            }
        }
        return result;
    }
    mapCommandsToTags() {
        for (const command of this.commands) {
            for (const keyword of command.keywords) {
                let tag = this.findTagByName(keyword);
                if (!tag) {
                    tag = new help_tag_1.HelpTag();
                    tag.name = keyword;
                    this.tags.push(tag);
                }
                tag.commands.push(command);
            }
        }
    }
    findTagByName(name) {
        for (const tag of this.tags) {
            if (tag.name.toLowerCase().trim() === name.toLowerCase().trim()) {
                return tag;
            }
        }
        return null;
    }
}
exports.HelpLoader = HelpLoader;
