export declare class Helpers {
    static findInArray(keys: string[], arr: string[]): boolean;
    static parseKeywords(helpString: string): string[];
    static parseCommand(helpString: string, robotName: string): string;
    static parseDescription(helpString: string, robotName: string): string;
    static parseParameters(helpString: string): object;
    static parseAliases(helpString: string, robotName: string): string[];
    static cleanCommand(command: string, robotName: string): string;
    static prepareToCompare(str: string): string;
    static format(str: string, variables: any): string;
}
