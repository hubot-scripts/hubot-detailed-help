import {HelpCommand} from "./help-command";
import {HelpTag} from "./help-tag";

export class HelpLoader {
    commands: HelpCommand[] = [];
    tags: HelpTag[] = [];
    robot: any = null;

    constructor(robot: any){
        this.robot = robot;

        const helpCommands: string[] = robot.helpCommands();
        for(const helpCommand of helpCommands){
            if(helpCommand.match(/^TAG\s+\[.*]/)){
                this.tags.push(new HelpTag(helpCommand));
            }
            else{
                this.commands.push(new HelpCommand(helpCommand, robot));
            }
        }
        this.mapCommandsToTags();
        robot.logger.debug(`Loaded ${this.commands.length} help commands and ${this.tags.length} help tags.`);
    }

    searchForCommand(key: string){
        let result = [];
        for(const command of this.commands){
            if(command.isQuery(key) && !this.findTagByName(key)){
                return [command];
            }
            if(command.matches(key)){
                result.push(command);
            }
        }
        return result;
    }

    private mapCommandsToTags(){
        for(const command of this.commands){
            for(const keyword of command.keywords){
                let tag = this.findTagByName(keyword);
                if(!tag){
                    tag = new HelpTag();
                    tag.name = keyword;
                    this.tags.push(tag);
                }
                tag.commands.push(command);
            }
        }
    }

    private findTagByName(name: string){
        for(const tag of this.tags){
            if(tag.name.toLowerCase().trim() === name.toLowerCase().trim()){
                return tag;
            }
        }
        return null;
    }
}