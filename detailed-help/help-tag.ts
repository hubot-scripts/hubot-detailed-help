import {HelpCommand} from "./help-command";
import {Helpers} from "./helpers";

export class HelpTag {
    name: string = '';
    description: string = '';
    commands: HelpCommand[] = [];

    constructor(tagString?: string) {
        if (tagString) {
            const finds = tagString.match(/^TAG\s+\[([^\[\]]*)]\s+[-:](.*)/);
            if (finds) {
                this.name = finds[1].trim();
                this.description = finds[2].trim();
            }
        }
    }

    display() {
        const format: string = process.env.HELP_TAG_FORMAT || '_${tag}_ (${commands}) - ${description}';
        return Helpers.format(format, {tag: this.name, description: this.description, commands: this.commands.length});
    }
}