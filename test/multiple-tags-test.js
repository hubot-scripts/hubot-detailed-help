'use strict';
const HubotChatTesting = require('hubot-chat-testing');
const Helper = require('hubot-test-helper');

describe('Testing the hubot help responses', () => {
    context('Testing the tags functionality', () => {
        const chat = new HubotChatTesting('hubot', new Helper(['./test-scripts/multiple-tags.js', '../scripts/detailed-help.js']));

        chat.when('user is asking for help that contains multiple tags')
            .setEnvironmentVariables({
                HUBOT_COLLAPSE_HELP: 'true'
            })
            .user('alice').messagesBot('help')
            .bot.messagesRoom(removeIndents(`
                Below you can find all of the commands that I know about. You can also narrow your search by adding some keywords - I will show you only those which you want to find. If only one command matches your query, I will display also more details about it :)
                I've **grouped the similiar commands by their tags** to make the help easier to read. If you want to show commands from the group, just use help TAG_NAME.
                The list of all possible tags:
                _COMMAND 1_ (2) - This tag is used to display some commands
                _HELP_ (2) - Commands used to display help
                _TEST_ (3) - This tag has description
                _COMMAND 2_ (1) - 
                _SPECIFIC_ (1) - 
                The list of commands that have no tag:
                _hubot untagged command_ - This command remains untagged because of some reason
            `))
            .expect('the bot should show the description for all tags');

        chat.when('user is asking the bot to show details about other tag')
            .setEnvironmentVariables({
                HUBOT_COLLAPSE_HELP: 'true'
            })
            .user('alice').messagesBot('help specific')
            .bot.messagesRoom('_hubot some command_ - Performs some command')
            .expect('the bot should show the commands with this tag');
    });
});

function removeIndents(string){
    return string.replace(/^ +/mg, "").trim();
}