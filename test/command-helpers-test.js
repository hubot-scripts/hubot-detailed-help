'use strict';
const expect = require('chai').expect;
const Helpers = require('../dist/helpers').Helpers;

describe('Testing the library helpers', () => {
    context('Finding the keywords in the array', () => {
        it('should find the keyword if all of the array strings contains it', () => {
            const array = ['The first alias of the command', 'The second alias of the command'];
            const keywords = ['alias', 'command'];
            const result = Helpers.findInArray(keywords, array);

            expect(result).to.be.true;
        });

        it('should find the keyword if any of the array strings contains it', () => {
            const array = ['The first alias of the command', 'The second alias of the command'];
            const keywords = ['alias', 'first'];
            const result = Helpers.findInArray(keywords, array);

            expect(result).to.be.true;
        });

        it('should not find the keyword if all of the array strings do not contain it', () => {
            const array = ['The first alias of the command', 'The second alias of the command'];
            const keywords = ['alias', 'third'];
            const result = Helpers.findInArray(keywords, array);

            expect(result).to.be.false;
        });
    });
});