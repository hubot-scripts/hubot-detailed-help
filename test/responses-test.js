'use strict';
const HubotChatTesting = require('hubot-chat-testing');
const Helper = require('hubot-test-helper');

describe('Testing the hubot help responses', () => {
    context('Modifying the bot responses via environment variables', () => {
        const chat = new HubotChatTesting('hubot', new Helper(['../scripts/detailed-help.js', './test-scripts/normal-help.js']));

        const modifiedResponses = {
            HELP_COMMAND_FORMAT: 'command: ${command}, description: ${description}',
            HUBOT_HELP_HEADER: 'This is Hubots help.',
            HUBOT_HELP_COLLAPSED_INFO: 'The commands are collapsed into tag groups.',
            HELP_TAG_FORMAT: 'Tag name: ${tag}, description: ${description}',
            HELP_PARAMETER_FORMAT: 'Parameter: ${parameter}, description: ${description}',
            HELP_PARAMETERS_HEADER: 'Changed params header:',
            HELP_ALIASES_HEADER: 'Changed aliases header:',
            HUBOT_HELP_NOT_FOUND: 'Changed not found summary for query:',
            HUBOT_HELP_TAGS_INFO: 'Changed tags summary:',
            HUBOT_HELP_UNTAGGED_INFO: 'Changed untagged commands summary:'
        };

        chat.when('displaying non-collapsed help summary')
            .setEnvironmentVariables({
                HUBOT_COLLAPSE_HELP: 'false',
                ...modifiedResponses
            })
            .user('alice').messagesBot('help')
            .bot.messagesRoom(removeIndents(`
                This is Hubots help.
                command: hubot help, description: Displays help
                command: hubot help \`QUERY\`, description: Searches help to find a command matching \`QUERY\`.
                command: hubot some command, description: Performs some command
                command: hubot some command \`QUERY\`, description: More complicated command
                command: hubot some untagged command, description: Untagged command
            `))
            .expect('the bot should use the new formats');

        chat.when('displaying collapsed help summary')
            .setEnvironmentVariables({
                HUBOT_COLLAPSE_HELP: 'true',
                ...modifiedResponses
            })
            .user('alice').messagesBot('help')
            .bot.messagesRoom(removeIndents(`
                This is Hubots help.
                The commands are collapsed into tag groups.
                Changed tags summary:
                Tag name: COMMAND, description: This tag is used to display some commands
                Tag name: HELP, description: Commands used to display help
                Tag name: TEST, description: This tag has description
                Changed untagged commands summary:
                command: hubot some untagged command, description: Untagged command
            `))
            .expect('the bot should use the new formats');

        chat.when('displaying search results when there are many commands')
            .setEnvironmentVariables(modifiedResponses)
            .user('alice').messagesBot('help command')
            .bot.messagesRoom(removeIndents(`
                command: hubot help \`QUERY\`, description: Searches help to find a command matching \`QUERY\`.
                command: hubot some command, description: Performs some command
                command: hubot some command \`QUERY\`, description: More complicated command
                command: hubot some untagged command, description: Untagged command
            `))
            .expect('the bot should use the new formats');

        chat.when('displaying search results when there is single command')
            .setEnvironmentVariables(modifiedResponses)
            .user('alice').messagesBot('help complicated command')
            .bot.messagesRoom(removeIndents(`
                command: hubot some command \`QUERY\`, description: More complicated command
                Changed params header:
                Parameter: QUERY, description: The query for the command
                Changed aliases header:
                hubot command \`QUERY\`
            `))
            .expect('the bot should use the new formats');

        chat.when('displaying search results when there is no command matching the provided query')
            .setEnvironmentVariables(modifiedResponses)
            .user('alice').messagesBot('help non-existing query')
            .bot.messagesRoom('Changed not found summary for query:non-existing query')
            .expect('the bot should use the new formats');
    });
});

function removeIndents(string){
    return string.replace(/^ +/mg, "").trim();
}